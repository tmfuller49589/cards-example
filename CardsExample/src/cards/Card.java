package cards;

/*
 * Java class for describing a card.
 * A card has a suit and a value.
 */

public class Card {
	private SuitEnum suit;
	private CardValueEnum value;

	/* 
	 * Create a card given a suit and a value.
	 */
	public Card(SuitEnum suit, CardValueEnum value) {
		this.suit = suit;
		this.value = value;
	}

	/* 
	 * Create a card without defining a suit or value.
	 */
	public Card() {
	}

	public SuitEnum getSuit() {
		return suit;
	}

	public void setSuit(SuitEnum suit) {
		this.suit = suit;
	}

	public int getValue() {
		return value.getValue();
	}

	public void setValue(CardValueEnum value) {
		this.value = value;
	}

	/*
	 * The toString method is invoked on any class if 
	 * the class is printed out.  For example:
	 * Card card = new Card(SuitEnum.Clubs, CardValueEnum.Ace);
	 * System.out.println(card);
	 * will print out Ace of Clubs
	 */
	@Override
	public String toString() {

		String s = value + " of " + suit;
		return s;
	}
}

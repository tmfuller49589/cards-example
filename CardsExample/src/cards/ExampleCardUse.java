package cards;

public class ExampleCardUse {

	public static void main(String[] args) {
		Deck deck = new Deck();
		System.out.println("original deck");
		deck.dump();

		System.out.println("shuffled deck");
		deck.shuffle();
		deck.dump();
	}
}

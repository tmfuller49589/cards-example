package cards;

/*
 * An enum is a java construct for describing a list of categories.
 * Each item in the CardValueEnum contains a card name and an integer value.
 * The card value is necessary for comparing cards - in some situations
 * we need to know which card is higher.
 */

public enum CardValueEnum {
	Two(2), Three(3), Four(4), Five(5), Six(6), Seven(7), Eight(8), Nine(9), Ten(10), Jack(11), Queen(12), King(
			13), Ace(14);

	private final int value;

	CardValueEnum(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}

}

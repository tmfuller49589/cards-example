package cards;

/*
 * An enum is a java construct for describing a list of categories.
 * In this case, the categories are the card suits.
 */
public enum SuitEnum {
	Hearts, Spades, Diamonds, Clubs;
}

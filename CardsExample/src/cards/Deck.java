package cards;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/*
 * Java class describing a deck of cards
 * 
 */
public class Deck {
	/*
	 * A deck is a ArrayList of cards.
	 * A List is a generic type - there are a few different kinds of lists:
	 * ArrayList and LinkedList are two.
	 * The type of item that the list holds is specified within the angle brackets.
	 *   new ArrayList<Card>(); creates an array list of cards.  The newly created
	 *   ArrayList is stored in the List named cards
	 */
	List<Card> cards = new ArrayList<Card>();

	/*
	 * Create a deck of cards and initialize it.
	 */
	public Deck() {
		initialize();
	}

	/*
	 * Retrieve the top card,
	 * remove it from the deck
	 * and return it to the caller.
	 */
	public Card getTopCard() {
		Card card = cards.get(0);
		cards.remove(0);
		return card;
	}

	/*
	 * Initialize the deck of cards.
	 * Create all card values in all suits.
	 */
	private void initialize() {
		// loop over the card suits
		for (SuitEnum suit : SuitEnum.values()) {
			// loop through the card values
			for (CardValueEnum value : CardValueEnum.values()) {
				Card card = new Card(suit, value); // create a new card
				cards.add(card); // add it to the list of cards
			}
		}
	}

	public void shuffle() {
		Collections.shuffle(cards);
	}

	/*
	 * Print out the list of cards in the deck.
	 */
	public void dump() {
		for (Card card : cards) {
			System.out.println(card);
		}
	}
}
